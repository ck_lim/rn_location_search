import { combineReducers } from "@reduxjs/toolkit";
import searchReducer from "../components/googleLocationSearch/reducer/searchReducer";

// add reducers
const rootReducer = combineReducers({
  search: searchReducer,
});

export default rootReducer;
