import React, { useState } from "react";
import { GoogleLocationSearch } from "../googleLocationSearch/GoogleLocationSearch";
import { GoogleMapView } from "../googleMapView/GoogleMapView";
import FabHistory from "../fabHistory";
import { HistoryList } from "../historyList/HistoryList";

export const Dashboard = () => {
  const [showHistory, setShowHistory] = useState(false);

  return (
    <>
      <GoogleLocationSearch />
      <GoogleMapView />
      <FabHistory onTap={() => setShowHistory(!showHistory)} />
      {showHistory && (
        <HistoryList onDismiss={() => setShowHistory(!showHistory)} />
      )}
    </>
  );
};
