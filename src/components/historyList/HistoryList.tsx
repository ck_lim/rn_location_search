import React, { useEffect, useState } from "react";
import { Modal, ScrollView, StyleSheet, Text, View } from "react-native";
import { useReduxDispatch, useReduxSelector } from "../../redux/store";
import { Button, List } from "@ant-design/react-native";
import { selectedResult } from "../googleLocationSearch/reducer/searchReducer";
import Item from "@ant-design/react-native/lib/list/ListItem";

interface IHistoryProps {
  onDismiss: () => void;
}

export const HistoryList: React.FC<IHistoryProps> = ({ onDismiss }) => {
  const dispatch = useReduxDispatch();
  // observe searched location history
  const selectedResults = useReduxSelector<any[]>((state) => state.search);

  // default to kuala lumpur
  const [historyList, setHistoryList] = useState<any[]>([]);

  useEffect(() => {
    // update map marker to latest searched location
    if (selectedResults.length > 0) {
      const historyListTemp: any[] = [];
      selectedResults.forEach((x) => historyListTemp.push(x));
      // reverse the list in descending order
      historyListTemp.reverse();
      setHistoryList(historyListTemp);
    }
  }, [selectedResults]);

  return (
    <Modal style={styles.modal} animationType="slide" onDismiss={onDismiss}>
      {historyList.length > 0 ? (
        <View style={styles.container}>
          <ScrollView
            style={styles.list}
            automaticallyAdjustContentInsets={false}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
          >
            <List renderHeader={"Search History"}>
              {historyList.map((x, index) => {
                return (
                  <Item
                    key={index}
                    onPress={(_) => {
                      onDismiss();
                      dispatch(selectedResult(x));
                    }}
                  >
                    {x.name}
                  </Item>
                );
              })}
            </List>
          </ScrollView>
        </View>
      ) : (
        <View style={styles.emptyList}>
          <Text style={styles.emptyListLabel}>History is empty.</Text>
        </View>
      )}
      <Button style={styles.btnClose} type="primary" onPress={onDismiss}>
        Close
      </Button>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    padding: 40,
  },
  container: {
    flex: 1,
  },
  emptyList: {
    flex: 1,
    alignContent: "center",
    justifyContent: "center",
    height: "100%",
    width: "100%",
  },
  emptyListLabel: {
    textAlign: "center",
  },
  btnClose: {
    justifyContent: "center",
    margin: 20,
    bottom: 20,
  },
  list: {
    ...StyleSheet.absoluteFillObject,
    height: "100%",
    width: "100%",
    backgroundColor: "#f5f5f9",
  },
});
