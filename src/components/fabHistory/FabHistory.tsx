import { historyIcon } from "../../image";
import React from "react";
import { Image, StyleSheet, TouchableOpacity } from "react-native";

interface IFabProps {
  onTap: () => void;
}

export const FabHistory: React.FC<IFabProps> = ({ onTap }) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={onTap}
      style={styles.touchableOpacityStyle}
    >
      <Image source={historyIcon} style={styles.floatingButtonStyle} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    padding: 10,
  },
  titleStyle: {
    fontSize: 28,
    fontWeight: "bold",
    textAlign: "center",
    padding: 10,
  },
  textStyle: {
    fontSize: 16,
    textAlign: "center",
    padding: 10,
  },
  touchableOpacityStyle: {
    position: "absolute",
    width: 50,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    right: 30,
    bottom: 45,
    borderRadius: 25,
    backgroundColor: "lightgray",
    // shadow
    shadowColor: "rgba(0,0,0, .4)", // IOS
    shadowOffset: { height: 5, width: 5 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 5, // Android
  },
  floatingButtonStyle: {
    resizeMode: "contain",
    width: 32,
    height: 32,
  },
});
