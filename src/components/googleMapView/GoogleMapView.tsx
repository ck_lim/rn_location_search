import React, { useEffect, useState } from "react";
import { StyleSheet, View } from "react-native";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import { useReduxSelector } from "../../redux/store";

export const GoogleMapView = () => {
  // observe searched location history
  const selectedResults = useReduxSelector<any[]>((state) => state.search);

  // default to kuala lumpur
  const [selectedLocation, setSelectedLocation] = useState({
    latitude: 3.140853,
    longitude: 101.693207,
    latitudeDelta: 0.015,
    longitudeDelta: 0.0121,
  });

  useEffect(() => {
    // update map marker to latest searched location
    if (selectedResults.length > 0) {
      const location = selectedResults[selectedResults.length - 1];
      setSelectedLocation({
        latitude: location.geometry.location.lat,
        longitude: location.geometry.location.lng,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
      });
    }
  }, [selectedResults]);

  return (
    <View style={styles.container}>
      <MapView
        provider={PROVIDER_GOOGLE}
        style={styles.map}
        region={selectedLocation}
      >
        <Marker coordinate={selectedLocation} />
      </MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    height: "100%",
    width: "100%",
  },
});
