import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export const searchSlice = createSlice({
  name: "search",
  initialState: <any>[],
  reducers: {
    selectedResult: (state, action: PayloadAction<any>) => {
      state = [...state, action.payload];
      return state;
    },
  },
});

export const { selectedResult } = searchSlice.actions;
export default searchSlice.reducer;
