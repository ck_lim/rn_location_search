import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import { Colors } from "react-native/Libraries/NewAppScreen";
import { useReduxDispatch } from "../../redux/store";
import { selectedResult } from "./reducer/searchReducer";
import { Toast } from "@ant-design/react-native";

export const GoogleLocationSearch = () => {
  const dispatch = useReduxDispatch();

  return (
    <View style={styles.container}>
      <GooglePlacesAutocomplete
        debounce={500}
        minLength={2}
        placeholder="Search"
        listViewDisplayed="auto"
        query={{
          key: "AIzaSyC_EBsvVAhc0CS7ygFcDJTMuy3UaFsgrkg",
        }}
        fetchDetails={true}
        onPress={(data, details = null) => {
          // 'details' is provided when fetchDetails = true
          // pass the location
          dispatch(selectedResult(details));
        }}
        onFail={(error) => (
          <View style={{ flex: 1 }}>
            <Text>{error}</Text>
          </View>
        )}
        onNotFound={() => Toast.info({ content: "No result were found!" })}
        listEmptyComponent={() => (
          <View style={{ flex: 1, margin: 10 }}>
            <Text>No results were found.</Text>
          </View>
        )}
        styles={{
          container: {
            zIndex: 10,
            overflow: "visible",
          },
          textInputContainer: {
            borderTopWidth: 0,
            borderBottomWidth: 0,
            height: 50,
            overflow: "visible",
            backgroundColor: Colors.white,
            borderColor: Colors.white,
            borderRadius: 100,
          },
          textInput: {
            backgroundColor: "transparent",
            fontSize: 15,
            lineHeight: 22.5,
            paddingBottom: 0,
            flex: 1,
          },
          listView: {
            position: "absolute",
            top: 60,
            left: 10,
            right: 10,
            backgroundColor: "white",
            borderRadius: 5,
            flex: 1,
            elevation: 3,
            zIndex: 10,
          },
          description: {
            color: "#1faadb",
          },
          predefinedPlacesDescription: {
            color: "#1faadb",
          },
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
});
